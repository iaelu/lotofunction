package p

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"time"
)

// LotoHandle creates a random FDJ Loto grid
func LotoHandle(w http.ResponseWriter, r *http.Request) {
	grid := loto()
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(&grid)
}

// Loto grid
type Loto struct {
	Nums   []int64
	Chance int64
}

func loto() Loto {
	rand.Seed(time.Now().UnixNano())
	mnums := make(map[int64]struct{})
	for len(mnums) != 5 {
		next := (rand.Int63() % 49) + 1
		if _, ok := mnums[next]; !ok {
			mnums[next] = struct{}{}
		}
	}
	nums := make([]int64, 0)
	for k := range mnums {
		nums = append(nums, k)
	}
	chance := (rand.Int63() % 10) + 1

	return Loto{Nums: nums, Chance: chance}
}
