package p

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestLotoHandle(t *testing.T) {
	req, _ := http.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()
	LotoHandle(rr, req)
	if rr.Body.String() == "" {
		t.Errorf("LotoHandle returns nothing")
	}
	var grid Loto
	if err := json.Unmarshal(rr.Body.Bytes(), &grid); err != nil {
		t.Errorf(fmt.Sprintf("could not unmarshal json: %q", err))
	}
	if len(grid.Nums) == 0 {
		t.Errorf("no grid numbers")
	}
	if grid.Chance == 0 {
		t.Errorf("no chance numbers")
	}
}

func BenchmarkLotoHandle(b *testing.B) {
	req, _ := http.NewRequest("GET", "/", nil)
	rr := httptest.NewRecorder()
	for n := 0; n < b.N; n++ {
		http.HandlerFunc(LotoHandle).ServeHTTP(rr, req)
	}
}
